#!/usr/bin/env python3
#-*- coding: utf-8 -*-

##This software is available to you under the terms of the GPL-3, see "/usr/share/common-licenses/GPL-3".
##Copyright:
##- Tomasz Makarewicz (makson96@gmail.com)

import os, tarfile, urllib.request, time
from subprocess import Popen, PIPE, call

from tools import sandbox

recultis_dir = os.getenv("HOME") + "/.recultis/"
gog_dir = recultis_dir + "shops/gog/"

def start(login, password, recultis_dir, g_appid, game_dir):
	shop_install_dir = recultis_dir + "shops/gog/"
	from tools import update_do, download_engine, unpack_deb
	print("Download lgogdownloader")
	lgog_link = update_do.get_link_list(["lgogdownloader"])[0]
	result = download_engine.download(lgog_link, recultis_dir + "tmp/lgogdownloader.deb")		
	unpack_deb.unpack_deb(recultis_dir + "tmp/", "lgogdownloader.deb")
	if sandbox.ispresent(shop_install_dir)[0]:
		sandbox.rm(shop_install_dir)
	sandbox.mkdir(shop_install_dir)
	sandbox.mv(recultis_dir + "tmp/lgogdownloader/bin/lgogdownloader", shop_install_dir + "lgogdownloader")
	print("Download innoextract")
	innoextract_link = update_do.get_link_list(["innoextract"])[0]
	result = download_engine.download(innoextract_link, recultis_dir + "tmp/innoextract.deb")		
	unpack_deb.unpack_deb(recultis_dir + "tmp/", "innoextract.deb")
	sandbox.mv(recultis_dir + "tmp/innoextract/bin/innoextract", shop_install_dir + "innoextract")
	print("Download cacert.pem")
	cacert_link = "https://curl.haxx.se/ca/cacert.pem"
	urllib.request.urlretrieve(cacert_link, shop_install_dir + "cacert.pem")
	print("Download game using lgogdownloader")
	os.chdir(shop_install_dir)
	rc = run_lgog(login, password, shop_install_dir, g_appid, game_dir)
	if rc == 1:
		rc = run_innoex(game_dir, shop_install_dir, g_appid)
	return rc

def run_lgog(login, password, shop_install_dir, g_appid, game_dir):
	if sandbox.ispresent(shop_install_dir+"gog_log.txt")[0]:
		sandbox.rm(shop_install_dir+"gog_log.txt")
	print("Running following gog command:")
	print("./lgogdownloader --download --game " + g_appid + " --directory " + game_dir + " --no-color --no-unicode --login-website --cacert cacert.pem")
	print("Check " + shop_install_dir + "gog_log.txt for more details.")
	env_var = "LD_LIBRARY_PATH=$HOME/.recultis/runtime/recultis2:$HOME/.recultis/runtime/recultis2/custom"
	gog_download = Popen(env_var + " stdbuf -oL -eL ./lgogdownloader --download --game " + g_appid + " --directory " + game_dir + " --no-color --no-unicode --login-website --cacert cacert.pem", shell=True, stdout=open("gog_log.txt", "wb"), stdin=PIPE, stderr=open("gog_log2.txt", "wb"))
	while gog_download.poll() is None:
		time.sleep(2)
		gog_error_line = get_last_error_line()
        #Insert Password
		if "Password" in gog_error_line:
			gog_download.stdin.write(bytes(password + '\n', 'ascii'))
			gog_download.stdin.flush()
		#Insert login
		elif "Email" in gog_error_line:
			gog_download.stdin.write(bytes(login + '\n', 'ascii'))
			gog_download.stdin.flush()
		#If computer is not registered on GOG, handle GOG Security code
		elif 'Security code' in gog_error_line:
			gog_guard_code = gog_guard()
			gog_download.stdin.write(bytes(gog_guard_code + '\n', 'ascii'))
			gog_download.stdin.flush()
	#if there is only 1 line after gog finished working, it means it crashed.
	if sum(1 for line in open('gog_log.txt')) == 1:
		rc = 0
	elif "error" in get_last_log_line():
		rc = 0
	elif "HTTP: Login failed" in get_last_error_line():
		rc = 0
	else:
		rc = 1
	return rc

def run_innoex(game_dir, shop_install_dir, g_appid):
	os.chdir(shop_install_dir)
	print("Running following innoextract command:")
	print("./innoextract " + game_dir + g_appid +"/setup*.exe --progress 1 -d " + game_dir)
	print("Check " + shop_install_dir + "gog_log.txt for more details.")
	env_var = "LD_LIBRARY_PATH=$HOME/.recultis/runtime/recultis2:$HOME/.recultis/runtime/recultis2/custom"
	innoextract_rc = call(env_var + " ./innoextract " + game_dir + g_appid +"/setup*.exe --progress 1 -d " + game_dir, shell=True, stdout=open("gog_log.txt", "ab"), stdin=PIPE, stderr=open("gog_log2.txt", "ab"))
	if innoextract_rc == 0:
		rc = 1
	else:
		rc = 0
	return rc

def get_last_log_line():
	if sandbox.ispresent("gog_log.txt")[0] == False:
		return ""
	gog_log_file = open("gog_log.txt", "r")
	gog_log_lines = gog_log_file.readlines()
	if len(gog_log_lines) > 0:
		gog_last_line = gog_log_lines[-1].rstrip()
		if gog_last_line == "" and len(gog_log_lines) > 1:
			gog_last_line = gog_log_lines[-2]
	else:
		gog_last_line = ""
	gog_log_file.close()
	return gog_last_line

def get_last_error_line():
	if sandbox.ispresent("gog_log2.txt")[0] == False:
		return ""
	gog_error_file = open("gog_log2.txt", "r")
	gog_error_lines = gog_error_file.readlines()
	if len(gog_error_lines) > 0:
		gog_last_error_line = gog_error_lines[-1].rstrip()
	else:
		gog_last_error_line = ""
	gog_error_file.close()
	return gog_last_error_line

def gog_guard():
	while sandbox.ispresent(recultis_dir + "guard_key.txt")[0] == False:
		time.sleep(2)
	print('GOG Security code detected. Verifying...')
	gog_guard_file = open(recultis_dir + "guard_key.txt", "r")
	gog_guard_code = gog_guard_file.readline()
	gog_guard_file.close()
	sandbox.rm(recultis_dir + "guard_key.txt")
	print(str(gog_guard_code).upper())
	return str(gog_guard_code.upper())
	
def status():
	status = "Downloading and installing game data"
	percent = 0
	if sandbox.ispresent(gog_dir + "gog_log.txt")[0] == False:
		return status, percent
	elif "failed" in get_last_error_line():
		return "Error: " + get_last_error_line() + ". Try again later.", 0
	for line in reversed(list(open(gog_dir + "gog_log.txt"))):
		if "%" in line:
			line = line.split("%")[0]
			line = line.split(" ")[-1]
			#lgogdownloader shows percentage without "." and innoextract with ".".
			if "." not in line:
				status = "Downloading game data"
				percent = int(int(line) * 0.9)
				break
			else:
				status = "Installing game data"
				percent = 90 + int(int(line.split(".")[0]) * 0.1)
				break
		elif "Done." in line:
			status = "Download of game data completed"
			percent = 100
			break
	return status, percent
			
