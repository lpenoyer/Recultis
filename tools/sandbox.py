#!/usr/bin/env python3
#-*- coding: utf-8 -*-

##This software is available to you under the terms of the GPL-3, see "/usr/share/common-licenses/GPL-3".
##Copyright:
##- Tomasz Makarewicz (makson96@gmail.com)

## This sandbox will handle files and dir operations to make sure that no system/user files are damaged. It has functions named after standard Linux commands.
## This sandbox is still missing archive extract, direct write to file (open) and file download operations.

import os, shutil

recultis_dir = os.getenv("HOME") + "/.recultis/"

def sandbox_check(target_path, sandbox_exceptions):
	sandboxed = 0
	allowed_paths = [recultis_dir]
	allowed_paths.extend(sandbox_exceptions)
	for allowed_path in allowed_paths:
		if target_path.startswith(allowed_path):
			sandboxed = 1
	return sandboxed

def rm(rm_path, sandbox_exceptions = []):
	result = 1
	if sandbox_check(rm_path, sandbox_exceptions):
		try:
			if os.path.islink(rm_path):
				os.unlink(rm_path)
			elif os.path.isfile(rm_path):
				os.remove(rm_path)
			elif os.path.isdir(rm_path):
				shutil.rmtree(rm_path)
			else:
				print("Warning. No such file or directory: " + rm_path)
				result = 0
		except:
			print("Warning. Remove operation failed: " + rm_path)
			result = 0
	else:
		print("Warning. File operation outside sandbox. Blocked.")
		result = 0
	return result

def mv(source_path, target_path, sandbox_exceptions = []):
	result = 1
	if sandbox_check(source_path, sandbox_exceptions) and sandbox_check(target_path, sandbox_exceptions):
		try:
			os.replace(source_path, target_path)
		except:
			print("Warning. Move operation failed: " + source_path + " to " + target_path)
			result = 0
	else:
		print("Warning. File operation outside sandbox. Blocked.")
		result = 0
	return result

def cp(source_path, target_path, sandbox_exceptions = []):
	result = 1
	if sandbox_check(target_path, sandbox_exceptions):
		try:
			if os.path.isfile(source_path):
				shutil.copy(source_path, target_path)
			elif os.path.isdir(source_path):
				shutil.copytree(source_path, target_path, symlinks=True)
			else:
				print("Warning. No such file or directory: " + source_path)
				result = 0
		except:
			print("Warning. Copy operation failed: " + source_path + " to " + target_path)
			result = 0
	else:
		print("Warning. File operation outside sandbox. Blocked.")
		result = 0
	return result

def mkdir(dir_path, sandbox_exceptions = []):
	result = 1
	if sandbox_check(dir_path, sandbox_exceptions):
		try:
			if os.path.isdir(dir_path) == False:
				os.makedirs(dir_path)
			else:
				print("Warning. Directory already present: " + dir_path)
				result = 0
		except:
			print("Warning. Make dir operation failed: " + dir_path)
			result = 0
	else:
		print("Warning. File operation outside sandbox. Blocked.")
		result = 0
	return result

def ln(source_path, target_path, sandbox_exceptions = []):
	result = 1
	if sandbox_check(target_path, sandbox_exceptions):
		try:
			os.symlink(source_path, target_path)
		except:
			print("Warning. Link operation failed: " + source_path + " to " + target_path)
			result = 0
	else:
		print("Warning. File operation outside sandbox. Blocked.")
		result = 0
	return result

def ispresent(present_path):
	if os.path.islink(present_path):
		result = 1
		p_type = "link"
	elif os.path.isfile(present_path):
		result = 1
		p_type = "file"
	elif os.path.isdir(present_path):
		result = 1
		p_type = "dir"
	else:
		result = 0
		p_type = "none"
	return result, p_type
